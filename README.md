# This is a test page only.

When the .tex file is updated, a pipeline automatically generates the following PDF:

[**A Test PDF File**](https://lomasp.gitlab.io/testing/test.pdf)

This is for testing purposes only.
This is a new line in testbranch.[^titlefn]

[^titlefn]: this is a footnote in the introduction.

[[_TOC_]]

# Heading 1

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line. <a name="cross-ref1"></a>

```math
a^2+b^2=c^2
```

This is a reference to [heading 1](#heading-1) and this is a reference to [heading 2](#heading-2).

This is a cross reference [to a previous line](#cross-ref1)

## Heading 2

```math
\sum_{i=1}^{10}TEST_{i,t}
```

This is a separate edit in master[^1]
[^1]: this is the text of footnote
